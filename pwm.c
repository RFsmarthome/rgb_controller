
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "pwm.h"

const uint16_t pwmtable[64] PROGMEM = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25,
	28, 30, 33, 36, 39, 42, 46, 49, 53, 56, 60, 64, 68, 72, 77, 81, 86, 90,
	95, 100, 105, 110, 116, 121, 127, 132, 138, 144, 150, 156, 163, 169,
	176, 182, 189, 196, 203, 210, 218, 225, 233, 240, 248, 255
};

void pwm_init()
{
	DDRD |= _BV(PD5) | _BV(PD6); // PWM Timer 0
	PORTD &= ~(_BV(PD5) | _BV(PD6));

	DDRB |= _BV(PB1); // PWM Timer 1
	PORTB &= ~_BV(PB1);

	TCCR0A = _BV(COM0A1) | _BV(COM0A0) | _BV(COM0B1)| _BV(COM0B0) | _BV(WGM01) | _BV(WGM00);
	TCCR0B = _BV(CS00);
	
	TCCR1A = _BV(COM1A1) | _BV(COM1A0) | _BV(WGM00);
	TCCR1B = _BV(WGM12) | _BV(CS10);

	OCR0A = 0;
	OCR0B = 0;
	OCR1A = 0;
}

void pwm_set_frequency(uint8_t channel, uint8_t level, uint8_t dimmer)
{
	uint16_t tmp = (pwm_read(level)*dimmer)/100;
	switch(channel) {
	case 0:
		OCR0A = tmp;
		break;
	case 1:
		OCR0B = tmp;
		break;
	case 2:
		OCR1A = tmp;
		break;
	}
}

uint16_t pwm_read(uint8_t level)
{
	return pgm_read_byte(&pwmtable[level & 0x3f]);
}

void pwm_set_dim_level(uint8_t channel, uint8_t level, uint8_t dimmer)
{
	pwm_set_frequency(channel, level*63/100, dimmer);
}
