#ifndef _PWM_H
#define _PWM_H

#include <avr/io.h>

#define TYPE_R 0
#define TYPE_G 2
#define TYPE_B 1

extern void pwm_init();
extern void pwm_set_dim_level(uint8_t channel, uint8_t level, uint8_t dimmer);
extern uint16_t pwm_read(uint8_t level);
extern void pwm_set_frequency(uint8_t channel, uint8_t level, uint8_t dimmer);

/*
class Pwm
{
public:
	void init() const;

	void setDimLevel(pwm_rgb_type channel, uint8_t level, uint8_t dimmer=100) const;

private:
	inline uint16_t readPwm(uint8_t level) const;
	inline void setPwmFreq(uint8_t channel, uint8_t level, uint8_t dimmer) const;
};
*/

#endif
