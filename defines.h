/*
 * defines.h
 *
 *  Created on: 20.11.2015
 *      Author: schnake
 */

#ifndef DEFINES_H_
#define DEFINES_H_


/*
 * LT89000
 */
#define CS_DDR DDRB
#define CS_PORT PORTB
#define CS_PIN PB2

#define RST_DDR DDRD
#define RST_PORT PORTD
#define RST_PIN PD4

#define PKT_DDR DDRD
#define PKT_PORT PORTD
#define PKT_SPIN PIND
#define PKT_PIN PD3



/*
 * DHT22
 */
/*
#define DDR_SENSOR   DDRD
#define PORT_SENSOR  PORTD
#define PIN_SENSOR   PIND
#define SENSOR       PD7
*/



/*
 * ADC0
 */
/*
#define LED_DDR DDRD
#define LED_PORT PORTD
#define LED_PIN PIND
#define LED_BIT PD6
*/



/*
 * LCD
 */
//  LCD DB4-DB7 <-->  PORTD Bit PD0-PD3
/*
#define LCD_PORT      PORTD
#define LCD_DDR       DDRD
#define LCD_DB        PD0

//  LCD RS      <-->  PORTD Bit PD4     (RS: 1=Data, 0=Command)
#define LCD_RS        PD4

//  LCD EN      <-->  PORTD Bit PD5     (EN: 1-Impuls für Daten)
#define LCD_EN        PD5
*/



/*
 * Station defines
 */
#define VCCREF 3.28

#endif /* DEFINES_H_ */
