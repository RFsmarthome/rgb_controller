/*
 * Crc8.cpp
 *
 *  Created on: 02.11.2015
 *      Author: schnake
 */

#include "crc8.h"


uint8_t crc_message(uint8_t polynom, const uint8_t *msg, uint8_t len) {
	uint8_t crc;
	crc = 0x00;

	while(len--) {
		crc ^= *msg++;

		for(uint8_t i = 8; i; i--) {
			if(crc & 0x80) {
				crc = (crc << 1) ^ polynom;
			} else {
				crc <<= 1;
			}
		}
	}

	return crc;
}
