/*
 * wdt.cpp
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#include <stdint.h>

#include "wdt.h"

#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>

uint8_t bits[] = {
		WDT_1SEC,
		WDT_2SEC,
		WDT_4SEC,
		WDT_8SEC
};

void wdt_powerdown(uint8_t timeout)
{
	cli();
	wdt_reset();
	MCUSR &= ~(1<<WDRF);
	WDTCSR = (1<<WDCE)|(1<<WDE);
	WDTCSR = _BV(WDE) | _BV(WDIE) | timeout;

	uint8_t adc_safe = ADCSRA;
	ADCSRA = 0;
	uint8_t prr_safe = PRR;
	PRR |= 0x01;

	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_bod_disable();
	sleep_enable();
	sei();
	sleep_cpu();
	sleep_disable();

	ADCSRA = adc_safe;
	PRR = prr_safe;
}

ISR(WDT_vect)
{
	wdt_disable();
}
