/*
 * wdt.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 *
 */

#ifndef WDT_H_
#define WDT_H_

#define WDT_16MS 0 // 16ms
#define WDT_32MS _BV(WDP0) // 32ms
#define WDT_64MS _BV(WDP1) // 64ms
#define WDT_0125SEC _BV(WDP1) | _BV(WDP0) // 0.125 sec
#define WDT_025SEC _BV(WDP02) // 0.25 sec
#define WDT_05SEC _BV(WDP2) | _BV(WDP0) // 0.5 sec
#define WDT_1SEC _BV(WDP1) | _BV(WDP2)	// 1 sec
#define WDT_2SEC _BV(WDP0) | _BV(WDP1) | _BV(WDP2) // 2 sec
#define WDT_4SEC _BV(WDP3) // 4 sec
#define WDT_8SEC _BV(WDP3) | _BV(WDP0) // 8 sec

extern void wdt_powerdown(uint8_t timeout);

#endif /* WDT_H_ */
