
#include <stdint.h>
#include <stdbool.h>

#include <avr/eeprom.h>
#include <util/delay.h>

#include "packet.h"
#include "crc8.h"
#include "lt8900.h"
#include "xtea.h"
#include "wdt.h"

bool packet_send(uint8_t address, struct buspacket_t *packet, bool wait)
{
	packet->packet = 0;
	packet->ack = 0;
	packet->req_ack = 0;

	packet->crc8 = crc_message(CRC_POLYNOM, (uint8_t*)packet, sizeof(struct buspacket_t)-1);

	bool rc = lt8900_send(address, packet, sizeof(struct buspacket_t), wait);
	wdt_powerdown(WDT_1SEC);
	return rc;
}


bool packet_sendAck(uint8_t source, struct buspacket_t *packet)
{
	packet->ack = 1;
	packet->req_ack = 0;

	uint8_t ack_dest = packet->source;
	packet->source = source;
	packet->crc8 = crc_message(CRC_POLYNOM, (uint8_t*)packet, sizeof(struct buspacket_t)-1);

	bool rc = lt8900_send(ack_dest, packet, sizeof(struct buspacket_t), true);
	_delay_us(500);
	return rc;
}

