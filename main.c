/*
 * pwm.cpp
 *
 * Created: 26.10.2015 17:56:46
 * Author : schnake
 */ 


#include <stdint.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

#include "schnakebus.h"

#include "defines.h"

#include "packet.h"
#include "pwm.h"
#include "lt8900.h"
#include "crc8.h"
#include "xtea.h"

uint8_t rf_address EEMEM = 3;

volatile uint8_t g_status;

#define STATIC_MODE 0
#define PROG_MODE 1

int main(void)
{
	cli();

	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	uint8_t dimmer = 100;
	uint8_t on_off = 1;
	uint8_t my_address = eeprom_read_byte(&rf_address);

	//uint8_t op_mode = STATIC_MODE;

	DDRB = 0x00;
	PORTB = 0xff;
	DDRC = 0x00;
	PORTC = 0xff;
	DDRD = 0x00;
	PORTD = 0xff;

	_delay_ms(30);

	ADCSRA &= ~_BV(ADEN);
	power_adc_disable();
	power_timer2_disable();
	power_twi_disable();
	power_usart0_disable();
	power_spi_enable();
	power_timer0_enable();
	power_timer1_enable();

	lt8900_init(7, 120, false);
	lt8900_reset();
	lt8900_init_hardware();
	lt8900_setPower(15, 4);

	// Enable external interrupt
	EICRA = _BV(ISC11) | _BV(ISC10);
	EIMSK = _BV(INT1);

	pwm_init();

	sei();

	struct buspacket_t packet;

	while(1) {
		// Begin listen
		lt8900_listen(my_address);

		// Sleep till interrupt 1 (LT8900 packet)
		cli();
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_enable();
		sei();
		sleep_cpu();
		sleep_disable();

		g_status = 0;

		// Datas receive
		if(lt8900_hasData()) {
			uint8_t length = lt8900_receive(&packet, sizeof(packet));
			if(packet.version==SB_VERSION) {
				// My address
				if(crc_message(CRC_POLYNOM, (uint8_t*)&packet, length)==0) {
					// CRC8 seems okay

					switch(packet.type) {
					case TYPE_RGB:
						// Package type RGB
						r = packet.rgb.r;
						g = packet.rgb.g;
						b = packet.rgb.b;
						break;
					case TYPE_DIMMER:
						// Package type DIMMER
						dimmer = packet.dimmer.dimmer;
						break;
					case TYPE_ON_OFF:
						on_off = packet.onoff.onoff;
						break;
					default:
						break;
					}

					if(on_off) {
						pwm_set_dim_level(TYPE_R, r, dimmer);
						pwm_set_dim_level(TYPE_G, g, dimmer);
						pwm_set_dim_level(TYPE_B, b, dimmer);
					} else {
						pwm_set_dim_level(TYPE_R, 0, 0);
						pwm_set_dim_level(TYPE_G, 0, 0);
						pwm_set_dim_level(TYPE_B, 0, 0);
					}

					/* Send ACK if receive REQ_ACK */
					if(packet.req_ack) {
						packet_sendAck(my_address, &packet);					}
				}
			}
		}
	}
}

ISR(INT1_vect)
{
	g_status = 1;
}
