#ifndef _PACKET_H
#define _PACKET_H

#include <stdint.h>
#include <stdbool.h>

#include "schnakebus.h"

bool packet_send(uint8_t address, struct buspacket_t *packet, bool wait);
bool packet_sendAck(uint8_t source, struct buspacket_t *packet);

#endif
